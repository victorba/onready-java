package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Car;

class CarTest {

	@Test
	void shouldHaveBrand() {
		Car car = new Car("Peugeot", "206", 4, 200000);
		assertEquals("Peugeot", car.getBrand());
	}
	
	@Test
	void shouldHaveModel() {
		Car car = new Car("Peugeot", "206", 4, 200000);
		assertEquals("206", car.getModel());
	}
	
	@Test
	void shouldHaveDoors() {
		Car car = new Car("Peugeot", "206", 4, 200000);
		assertEquals(4, car.getNumberOfDoors());
	}
	
	@Test
	void shouldHaveAPrice() {
		Car car = new Car("Peugeot", "206", 4, 200000);
		assertEquals("$200.000,00", car.getPrice());
	}
	
	@Test
	void shouldObtainInfo() {
		Car car = new Car("Peugeot", "206", 4, 200000);		
		String expectedInfo = "Marca: Peugeot // Modelo: 206 // Puertas: 4 // Precio: $200.000,00";
		assertEquals(expectedInfo, car.getInfo());
	}
	
	@Test
	void testShouldReturnBrandAndModel() {
		Car car = new Car("Peugeot", "206", 4, 200000);		
		assertEquals("Peugeot 206", car.getBrandModel());
	}
}
