package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Car;
import model.Motorcycle;
import model.Vehicle;

class VehicleTest {

	@Test
	void testShouldCompareThemByPrice() {
		Vehicle motorcycle = new Motorcycle("Honda", "Titan", 125, 60000);
		Vehicle car = new Car("Peugeot", "206", 4, 200000);
		assertEquals(1, car.compareTo(motorcycle));
	}
}
