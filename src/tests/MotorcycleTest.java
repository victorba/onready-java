package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Motorcycle;

class MotorcycleTest {

	@Test
	void testShouldHaveCylinders() {
		Motorcycle motorcycle = new Motorcycle("Honda", "Titan", 125, 60000);
		assertEquals("125c", motorcycle.getCylinder());
	}
	
	@Test
	void testShouldObtainInfo() {
		Motorcycle motorcycle = new Motorcycle("Honda", "Titan", 125, 60000);
		String expectedInfo = "Marca: Honda // Modelo: Titan // Cilindrada: 125c // Precio: $60.000,00";
		assertEquals(expectedInfo, motorcycle.getInfo());
	}
}
