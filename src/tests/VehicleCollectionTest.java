package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import model.Car;
import model.Motorcycle;
import model.Vehicle;
import model.VehicleCollection;

class VehicleCollectionTest {

	@Test
	void testShouldReturnTheMostExpensiveVehicle() {
		VehicleCollection collection = new VehicleCollection();
		collection.add(new Motorcycle("Honda", "Titan", 125, 60000));
		collection.add(new Car("Peugeot", "206", 4, 200000));
		assertEquals("Peugeot", collection.getMostExpensive().get().getBrand());
	}
	
	@Test
	void testShouldReturnTheCheapestVehicle() {
		VehicleCollection collection = new VehicleCollection();
		collection.add(new Motorcycle("Honda", "Titan", 125, 60000));
		collection.add(new Car("Peugeot", "206", 4, 200000));
		assertEquals("Honda", collection.getCheapest().get().getBrand());
	}
	
	@Test
	void testShouldReturnTheOneThatContainsSomeCharacterInItsModel() {
		VehicleCollection collection = new VehicleCollection();
		collection.add(new Motorcycle("Honda", "Titan", 125, 60000));
		collection.add(new Car("Peugeot", "206", 4, 200000));
		collection.add(new Car("Peugeot", "208", 5, 250000));
		collection.add(new Motorcycle("Yamaha", "YBR", 160, 80500.50));

		assertEquals("Yamaha", collection.findCharInBrand('Y').get().getBrand());
	}
	 
	@Test
	void testShouldSortVehicles() {
		VehicleCollection collection = new VehicleCollection();
		Vehicle honda_titan = new Motorcycle("Honda", "Titan", 125, 60000);
		Vehicle peugot_206 = new Car("Peugeot", "206", 4, 200000);
		Vehicle peugot_208 = new Car("Peugeot", "208", 5, 250000);
		Vehicle yamaha_ybr = new Motorcycle("Yamaha", "YBR", 160, 80500.50);
		collection.add(honda_titan);
		collection.add(peugot_206);
		collection.add(peugot_208);
		collection.add(yamaha_ybr);	
		
		collection.descendingSort();
		
		List<Vehicle> expected = Arrays.asList(peugot_208, peugot_206, yamaha_ybr, honda_titan);
		assertTrue(collection.asList().equals(expected));
	}
}
