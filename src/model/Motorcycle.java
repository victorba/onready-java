package model;

public class Motorcycle extends Vehicle {
	int cylinder;
	
	public Motorcycle(String brand, String model, int cylinder, double price) {
		super(brand, model, price);
		this.cylinder = cylinder;
	}
	
	public String getCylinder() {
		return String.format("%dc", this.cylinder);
	}
	
	public String getInfo() {
		return this.getInfoPlus(String.format(" Cilindrada: %s ", this.getCylinder()));
	}
}
