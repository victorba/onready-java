package model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class Vehicle implements Comparable<Vehicle> {
	String brand;
	String model;
	double price;
	String PRICE_PATTERN = "$###,###.00";
	Locale LOCALE = new Locale("es", "AR");
	protected String DELI = "//";
	protected int EXTRA_INFO_POS = 2;
	
	public Vehicle(String brand, String model, double price) {
		this.brand = brand;
		this.model = model;
		this.price = price;
	}
	
	public String getBrand() {
		return this.brand;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public String getBrandModel() {
		return String.format("%s %s", this.brand, this.model);
	}
	
	public String getPrice() {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(this.LOCALE);
        decimalFormat.applyPattern(this.PRICE_PATTERN);
        return decimalFormat.format(this.price);
	}
	
	@Override
	public int compareTo(Vehicle other) {
		if (this.price > other.price) {
			return 1;
		} else if (this.price == other.price) {
			return 0;
		} else {
			return -1;
		}
	}
	
	public abstract String getInfo();
	
	protected List<String> listInfo() {
		List<String> info = new ArrayList<String>();
		info.add(String.format("Marca: %s ", this.getBrand()));
		info.add(String.format(" Modelo: %s ", this.getModel()));
		info.add(String.format(" Precio: %s", this.getPrice()));
		return info;
	}
	
	protected String getInfoPlus(String newInfo) {
		List<String> info = this.listInfo();
		info.add(this.EXTRA_INFO_POS, newInfo);
		return String.join(this.DELI, info);
	}
}
