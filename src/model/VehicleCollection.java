package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class VehicleCollection {
	List<Vehicle> collection = new ArrayList<Vehicle>();

	public void add(Vehicle vehicle) {
		this.collection.add(vehicle);
	}

	public Optional<Vehicle> getMostExpensive() {
		return this.collection.stream().max((i, j) -> i.compareTo(j));
	}

	public Optional<Vehicle> getCheapest() {
		return this.collection.stream().min((i, j) -> i.compareTo(j));
	}

	public Optional<Vehicle> findCharInBrand(char c) {
		return this.collection.stream()
							  .filter(v -> v.getBrand().contains(String.valueOf(c)))
							  .findFirst();
	}

	public void descendingSort() {
		Collections.sort(this.collection, Collections.reverseOrder()); 	
    }

	public List<Vehicle> asList() {
		return this.collection;
	}
}
