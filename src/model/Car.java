package model;

public class Car extends Vehicle {
	int numberOfDoors;
	
	public Car(String brand, String model, int numberOfDoors, double price) {
		super(brand, model, price);
		this.numberOfDoors = numberOfDoors;
	}

	public int getNumberOfDoors() {
		return this.numberOfDoors;
	}

	public String getInfo() {
		return this.getInfoPlus(String.format(" Puertas: %s " , this.getNumberOfDoors()));
	}
}
