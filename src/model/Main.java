package model;

public class Main {

	public static void main(String[] args) {
		VehicleCollection collection = Main.loadCollection();
		
		collection.asList().stream().forEach(vehicle -> System.out.println(vehicle.getInfo()));
		Main.printDelim();
		
		Main.printVehicle("Vehículo más caro: ", collection.getMostExpensive().get());
		Main.printVehicle("Vehículo más barato: ", collection.getCheapest().get());
		Main.printVehicleWithChar("Vehículo que contiene en el modelo la letra ‘Y’: ", 
								  collection.findCharInBrand('Y').get());
		Main.printDelim();
		
		collection.descendingSort();
		Main.printAllVehicles(collection);
	}
	
	public static VehicleCollection loadCollection() {
		VehicleCollection collection = new VehicleCollection();
		collection.add(new Motorcycle("Honda", "Titan", 125, 60000));
		collection.add(new Car("Peugeot", "206", 4, 200000));
		collection.add(new Car("Peugeot", "208", 5, 250000));
		collection.add(new Motorcycle("Yamaha", "YBR", 160, 80500.50));	
		return collection;
	}
	
	public static void printDelim() {
		System.out.println("=============================");
	}
	
	public static void printVehicle(String text, Vehicle vehicle) {
		System.out.println(text + vehicle.getBrandModel());
	}
	
	public static void printVehicleWithChar(String text, Vehicle vehicle) {
		System.out.println(String.format("%s %s %s", 
										 text,
										 vehicle.getBrandModel(),
										 vehicle.getPrice()));
	}	
	public static void printAllVehicles(VehicleCollection collection) {
		collection
			.asList()
		    .stream()
		    .forEach(vehicle -> System.out.println(vehicle.getBrandModel()));	
	}
}
